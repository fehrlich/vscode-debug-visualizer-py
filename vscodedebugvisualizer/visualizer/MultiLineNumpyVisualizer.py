import numpy as np
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from vscodedebugvisualizer.visualizer.PlotlyVisualizer import PlotlyVisualizer


class MultiLineNumpyVisualizer(PlotlyVisualizer):
    def __init__(self) -> None:
        super().__init__()
        self.maxChannels = 25
        self.sharedXAxis = True
        self.labels = None
        self.dtype = np.float16

    def checkType(self, t):
        return isinstance(t, (np.ndarray, list))

    def selectAndTailorData(self, data: np.ndarray | list):
        maxLength = 800
        # maxChannels = self.maxChannels

        lines = []
        for i, line in enumerate(data):
            if i >= self.maxChannels:
                break
            lineLength = len(line)
            factor = lineLength // maxLength if lineLength > maxLength else 1

            # line = np.arange(0, shape[1], factor)
            line = line.astype(self.dtype)
            lines.append(line[::factor])
        return lines

    def multiScatterView(self, lines):
        lineCount = len(lines)

        fig = make_subplots(
            rows=lineCount,
            cols=1,
            shared_xaxes=self.sharedXAxis,
            specs=[[{"type": "scattergl"}]] * lineCount,
        )

        for i, y in enumerate(lines):
            name = self.labels[i] if self.labels else "lineName %i"
            row = i + 1
            fig.add_trace(
                go.Scatter(
                    # x=xValues,
                    y=y,
                    mode="lines",
                    name=name,
                    legendgrouptitle={"text": "y[%i]" % i},
                    legendgroup="%i" % i,
                ),
                row=row,
                col=1,
            )

        return fig

    def visualize(self, lines):
        lines = self.selectAndTailorData(lines)
        fig = self.multiScatterView(lines)

        return super().visualize(fig)
